import os
import re
import string
import random
import requests
from bs4 import BeautifulSoup


def boundary_id(size=16, chars=string.hexdigits):
    return ''.join(random.choice(chars) for _ in range(size))


def html_parser(source, css):
    html = BeautifulSoup(source)
    return html.select(css)

        
class Gokano(object):
    VERSION = '0.1.2'

    def __init__(self, email, password):
        if email == "":
            print("missing email address")
            raise SystemExit(0)
        elif password == "":
            print("missing password")
            raise SystemExit(0)
        else:
            self.email = email
            self.password = password
            self.gokano_session = requests.Session()
            self.authenticated = False

    @staticmethod
    def bot_headers():
        return {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'en-US,en;q=0.8,th;q=0.6',
            'Cache-Control': 'max-age=0',
            'Connection': 'keep-alive',
            'DNT': '1',
            'Host': 'gokano.com',
            'Origin': 'http://gokano.com',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36'
        }

    @staticmethod
    def get_username(source):
        gokano_username = html_parser(source.text, 'div.username > p')

        if len(gokano_username) == 0:
            print("parsing error: couldn't find username")
            raise SystemExit(0)
        else:
            return gokano_username[0].get_text()

    @staticmethod
    def get_points(source):
        gokano_points = html_parser(source.text, 'div.points > p')

        if len(gokano_points) == 0:
            print("parsing error: couldn't find points")
            raise SystemExit(0)
        else:
            points = re.search('\d+', gokano_points[0].get_text())

            return points.group(0)

    @staticmethod
    def get_total_referrals(source):
        total_referrals = html_parser(source.text, 'p.friends')

        if len(total_referrals) == 0:
            print("parsing error: couldn't find referrals")
            raise SystemExit(0)
        else:
            points = re.search('\d+', total_referrals[0].get_text())

            return points.group(0)

    @staticmethod
    def get_next_ranking(source):
        next_ranking = html_parser(source.text, 'p.nextRank')

        if len(next_ranking) == 0:
            print("parsing error: couldn't find referrals")
            raise SystemExit(0)
        else:
            points = re.search('\d+', next_ranking[0].get_text())

            return points.group(0)

    @staticmethod
    def get_current_ranking(source):
        gokano_username = html_parser(source.text, 'div.currentStatus')

        if len(gokano_username) == 0:
            print("parsing error: couldn't find username")
            raise SystemExit(0)
        else:
            return gokano_username[0].get_text().replace('\n', '')

    @staticmethod
    def get_login_response(source):
        gokano_login_response = html_parser(source.text, '.gokan-alert-error')

        if len(gokano_login_response) > 0:
            print("login error: {0}".format(gokano_login_response[0].get_text()))
            raise SystemExit(0)

    @staticmethod
    def get_item_status(source):
        prize_status = html_parser(source.text, 'div.saveData')

        if len(prize_status) == 0:
            print("parsing error: couldn't find prize")
            raise SystemExit(0)
        else:
            return prize_status[0].get_text()

    @staticmethod
    def get_mission_data(source):
        gokano_mission = html_parser(source.text, 'form')
        gokano_mission_token = html_parser(source.text, 'input[name="_token"]')
        gokano_mission_answer = html_parser(source.text, 'input[name="response"]')

        if len(gokano_mission) == 0:
            print("mission complete")
            raise SystemExit(0)
        else:
            if len(gokano_mission_token) == 0:
                print("mission complete")
                raise SystemExit(0)
            if len(gokano_mission_answer) == 0:
                print("mission complete")
                raise SystemExit(0)
            return {
                'url': gokano_mission[0]['action'],
                'token': gokano_mission_token[0]['value'],
                'response': random.choice(gokano_mission_answer)['value']
            }

    @staticmethod
    def save_session(session_key):
        with open('session.txt', 'w') as session_file:
            session_file.write(session_key)

    @staticmethod
    def read_session():
        with open('session.txt') as session_file:
            return session_file.read()

    @staticmethod
    def file_exists():
        return os.path.exists('session.txt')

    def is_authenticated(self):
        return self.authenticated

    def update_headers(self, new_headers):
        headers = self.bot_headers()

        for key, value in new_headers.items():
            headers[key] = value
        return headers

    def login(self):
        login_data = {
            'email': self.email,
            'password': self.password
        }
        login_headers = self.update_headers({
            'Referer': 'http://gokano.com/',
            'Content-Type': 'application/x-www-form-urlencoded'
        })
        source = self.gokano_session.post('http://gokano.com/login', data=login_data, headers=login_headers)
        self.save_session('laravel_session={0};'.format(dict(source.cookies)['laravel_session']))
        
        if 'dashboard' in source.url:
            self.authenticated = True
            print('Name: {0}'.format(self.get_username(source)))
            print('Points: {0}'.format(self.get_points(source)))
            print('Total Referrals: {0}'.format(self.get_total_referrals(source)))
            print('Next Ranking: {0}'.format(self.get_next_ranking(source)))
            print('Current Rank: {0}'.format(self.get_current_ranking(source)))
        else:
            self.get_login_response(source)

    def total_points(self):
        total_points_header = self.update_headers({
            'Referer': 'http://gokano.com/',
            'Cookie': self.read_session()
        })
        source = self.gokano_session.get('http://gokano.com/dashboard', headers=total_points_header)
        return self.get_points(source)

    def daily_points(self):
        daily_points_headers = self.update_headers({
            'Referer': 'http://gokano.com/dashboard',
            'Cookie': self.read_session()
        })

        self.gokano_session.get('http://gokano.com/daily', headers=daily_points_headers)
        print("done daily")

    def daily_mission(self):
        daily_mission_headers = self.update_headers({
            'Referer': 'http://gokano.com/dashboard',
            'Cookie': self.read_session()
        })
        mission_source = self.gokano_session.get('http://gokano.com/missions', headers=daily_mission_headers)
        mission_data = self.get_mission_data(mission_source)
        daily_mission_headers['Referer'] = 'http://gokano.com/missions'
        daily_mission_headers['Content-Type'] = 'multipart/form-data; boundary=----WebKitFormBoundary{0}'.format(boundary_id())

        self.gokano_session.post(mission_data['url'], params={ '_token': mission_data['token'], 'response': mission_data['response'] }, headers=daily_mission_headers)
        print("done mission")

    def check_status(self, prize_ids):
        prizes = {}
        for prize_id in prize_ids:
            prize_headers = self.update_headers({
                'Referer': 'http://gokano.com/prize/{0}'.format(prize_id),
                'Cookie': self.read_session()
            })
            source = self.gokano_session.get('http://gokano.com/prize/{0}'.format(prize_id), headers=prize_headers)
            prizes[prize_id] = self.get_item_status(source)
        return prizes










